# rmusic

This script copies a number of random files of a directory into another directory. 
A possible use is to have some random songs in a directory and sync it with another device (a phone, a tablet...)

## Usage

Download:
    
    wget -O /usr/local/bin/rmusic https://gitlab.com/dunetna/rmusic/raw/master/rmusic.py

Edit the script and change the value of the following variables:
 * songs_dir: path to the music directory
 * random_dir: path to the output directory
 * n_songs: number of songs you want in output directory
 * excludes: list of filenames that you don't want to be removed of output directory

Run:

    ./rmusic.py 
