#!/usr/bin/env python3

# Copy songs randomly to a directory

import os
import random
import shutil

songs_dir = "path_to_music"
random_dir = "path_to_random_music"
n_songs = 5
excludes = [".stfolder"]
songs = []

# Put all the song full names in a list
for root, directories, filenames in os.walk(songs_dir):
	for name in filenames:
		songs.append(os.path.join(root, name));

# Shuflle the list songs
random.shuffle(songs)

# Leave random directory empty
for root, dirs, files in os.walk(random_dir):
	for name in files:
		if name not in excludes:
			os.remove(os.path.join(root, name))
        
# Copy the first n_songs of shuffled list into random directory
i = 1
for s in songs[:n_songs]:
	# Copy the song into the random directory renaming it with a prefix, 
	# an incremental integer
	shutil.copy(s, random_dir + "/" + str(i) + "-" + s.split("/")[-1])
	i = i + 1
